### Issue [Cannot redeclare block scoped variable]
Add `export {}` To the end of ts file to convert this scope (global scope) to Module Scope


### Notice Send Mail Template
Gmail / or others mail server will remove <html> <body> <head> and <script> tags for security
They just retain html content and we have to use inline style to makeup for email content
