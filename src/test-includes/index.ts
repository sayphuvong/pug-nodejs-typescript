const pug = require('pug');
const path = require('path');
const compiledFunction = pug.compileFile(path.resolve(__dirname, 'template/index.pug'));

// Test Includes
console.log(compiledFunction());

// export {} To convert be Module Scope
export {};
