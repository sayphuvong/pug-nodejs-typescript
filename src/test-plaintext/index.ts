const pug = require("pug");
const path = require("path");
const juice = require("juice");

const compiledFunction = pug.compileFile(
  path.resolve(__dirname, "template/index.pug")
);

const templateHtml = compiledFunction({
  verifyUrl: "https://google.com",
});

const templateHtmlWithInlineStyle = juice(templateHtml);

console.log(templateHtmlWithInlineStyle);

const message = {
  app_id: "0817e648-ae0f-4544-9fce-1aabcd3e6a36",
  include_email_tokens: ["sayphuvong@gmail.com", "phuvs@gamifystudios.co"],
  email_subject: "Welcome to Cat Facts!",
  email_body: templateHtmlWithInlineStyle,
};

const sendNotification = function (data: typeof message) {
  const headers = {
    "Content-Type": "application/json; charset=utf-8",
    Authorization: "Basic ZGExYTFmODItNzdiMC00YWIxLWE3MGMtM2IzNjMwMzdjYmQz",
  };

  const options = {
    host: "onesignal.com",
    port: 443,
    path: "/api/v1/notifications",
    method: "POST",
    headers: headers,
  };

  const https = require("https");
  const req = https.request(options, function (res: any) {
    res.on("data", function (data: string) {
      console.log("RESPONSE:\n", JSON.parse(data));
    });
  });

  req.on("error", function (err: Error) {
    console.log("ERROR:\n", err);
  });

  req.write(JSON.stringify(data));
  req.end();
};

sendNotification(message);

// export {} To convert be Module Scope
export {};
